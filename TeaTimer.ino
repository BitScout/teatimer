
#include <Servo.h>

#define PIN_BUTTON 2
#define PIN_SERVO  3

Servo servo;

const int DELAY     = 20;
const int INCREMENT = 1;

#define STATE_UP         10
#define STATE_DOWN       11
#define STATE_GOING_UP   20
#define STATE_GOING_DOWN 21

const int POSITION_UP   = 230;
const int POSITION_DOWN = 20;

volatile int state = STATE_UP;
int position = POSITION_UP;
long timer = 0;
long duration = 2000;

int lastButtonState = LOW;   // the previous reading from the input pin

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0; // the last time the output pin was toggled
unsigned long debounceDelay = 50;   // the debounce time; increase if the output flickers

void setup() {
  pinMode(PIN_BUTTON, INPUT_PULLUP);
  
  servo.attach(PIN_SERVO);
  servo.write(POSITION_UP);
  delay(100);
  servo.detach();
  
  attachInterrupt(digitalPinToInterrupt(PIN_BUTTON), myISR, FALLING);
}

void loop() {
  delay(DELAY);

  if(state == STATE_GOING_DOWN) {
    if(position > POSITION_DOWN) {
      position -= INCREMENT;
    } else {
      state = STATE_DOWN;
      servo.detach();
    }
  } else if(state == STATE_GOING_UP) {
    if(position < POSITION_UP) {
      position += INCREMENT;
    } else {
      state = STATE_UP;
      servo.detach();
    }
  }

  if(state == STATE_DOWN) {
    timer -= DELAY;
    Serial.println(timer);
    
    if(timer <= 0) {
      state = STATE_GOING_UP;
      servo.attach(PIN_SERVO);
    }
  }

  // Actually move servo
  servo.write(position);
}

void myISR() {
  if(state != STATE_UP) {
    return;
  }
  
  timer = duration;
  state = STATE_GOING_DOWN;
  servo.attach(PIN_SERVO);
}
